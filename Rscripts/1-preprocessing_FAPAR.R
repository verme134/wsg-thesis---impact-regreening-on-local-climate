# In this script NETCDF files are stacked, cropped, low quality pixels are filtered out
# Input: netcdf files
# Output: stacked data, file containing dates corresponding to data

rm(list = ls())  # clear workspace

# Install packages and load libraries -------------------------------------

# install packages if required 
if(!require(raster)){install.packages("raster")}
if(!require(ncdf4)){install.packages("ncdf4")}
if(!require(lubridate)){install.packages("lubridate")}

# load libraries
library(raster) # package for raster manipulation
library(ncdf4) # package for netcdf manipulation 
library(lubridate) # package to handle dates more easily

# check working directory and untar files ---------------------------------

workdirdata <- "XXX"

setwd(workdirdata) # set working directory correctly

tgzfiles <- list.files(path = workdirdata, pattern = '.tar.gz$') # list of tarballs in data directory; one per year FAPAR data.

for(i in 1:length(tgzfiles)) {
  # untar tarballs
  untar(tgzfiles[i])
}

# Now the netCDF files are extracted
# Data has following structure:
# 10 daily composite data. Each month has three dates (nominal days)
# Each day has a few (2  or 3) separate netCDF files # THIS WAS A BUG IN THE COPERNICUS CLIMATE DATA STORE THAT HAS NOW BEEN FIXED
# However, only one of these files actually contains data
# The other are of different versions and can somehow not be opened.
# Therefore I move them to another directory using the following Linux command:
# find /lustre/backup/WUR/ESG/verme134/data/fapar -maxdepth 1 -type 'f' -name "*.nc" -size -8000c -exec mv {} /lustre/backup/WUR/ESG/verme134/data/fapar/smallfiles/ \;
# Now all the useless files are in another directory: small files
# and workdirdata only contains useful files.

# Because the files are verly large, it is better to crop them with Climate Data Operators (CDO)
# I use the shell script: cropwithcdo.sh for this.
# Then I move all the large global files, to another working directory using the Linux command:
# find /lustre/backup/WUR/ESG/verme134/data/fapar -maxdepth 1 -type 'f' -name "*.nc" -size +60000000c -exec mv {} /lustre/backup/WUR/ESG/verme134/data/fapar/globalfiles/ \;
# Now the workdirdata only contains cropped files

# Create rasterstacks -----------------------------------------------------

ncfiles <- list.files(path = workdirdata, pattern = '.nc$') # list of ncfiles in data directory; one per year FAPAR data.

# Dates are not preserved when the brick is converted to the rasterstack.
# Therefore, the dates are stored in a dataframe
# Create empty dataframe to store dates in:
date_df <- data.frame(Date=rep(0,length(ncfiles)),
                      Year=rep(0,length(ncfiles)),Month=rep(0,length(ncfiles)))

#  Make a rasterstack of the fapar data
FAPAR_stack <- stack()
for(i in 1:length(ncfiles)) {
  # create a rasterbrick
  FAPAR_brick <- brick(ncfiles[i] , varname = "FAPAR")
  # Add date to dataframe
  date <- getZ(FAPAR_brick)
  year <- lubridate::year(date)
  month <- lubridate::month(date)
  date_df[i, ]=c(date,year,month)
  # create raster stack
  FAPAR_stack <- stack(FAPAR_stack, FAPAR_brick)
}

rm(FAPAR_brick,date,year,month)  # remove unused variables from workspace

saveRDS(date_df,file="date_df.Rda")

#  Make a rasterstack of the fapar quality flag data
FAPAR_QFLAG_stack <- stack()
for(i in 1:length(ncfiles)) {
  # create a rasterbrick
  FAPAR_QFLAG_brick <- brick(ncfiles[i] , varname = "FAPAR_QFLAG")
  # create raster stack
  FAPAR_QFLAG_stack <- stack(FAPAR_QFLAG_stack, FAPAR_QFLAG_brick)
}

rm(FAPAR_QFLAG_brick)  # remove unused variables from workspace

# Now save the rasterstacks
writeRaster(FAPAR_stack, "FAPAR.grd", overwrite=TRUE)
writeRaster(FAPAR_QFLAG_stack, "FAPAR_QFLAG.grd", overwrite=TRUE)

# Load the rasterstacks - if R session was terminated in the meantime
FAPAR_stack <- stack("FAPAR.grd")
FAPAR_QFLAG_stack <- stack("FAPAR_QFLAG.grd")

# Mask out low quality pixels ---------------------------------------------

# Use quality flag stack to mask out low quality pixels
# All values that are not 0 in the quality flag stack (i.e. have a flag value) are masked out in the fapar stack
FAPAR_clean <- mask(FAPAR_stack, FAPAR_QFLAG_stack, filename="FAPAR_clean.grd",
                    inverse=TRUE, maskvalue=0, updatevalue=NA)

# Mask to China extent ----------------------------------------------------

# Mask out all values that are outside of China extent
China_extent <- getData("GADM",country="China",level=0)
FAPAR_clean_China <- mask(FAPAR_clean, China_extent, filename="FAPAR_clean_China.grd")

rm(FAPAR_stack, FAPAR_QFLAG_stack, FAPAR_clean)  # remove unused variables from workspace

# Applying the scaling factor to the data value to get the physical value or setting missing values to NA is not necessary.
# This is automatically done when the raster package opens the netCDF file.