#!/bin/bash
#-----------------------------Mail address-----------------------------
#SBATCH --mail-user=XXX
#SBATCH --mail-type=ALL
#-----------------------------Output files-----------------------------
#SBATCH --output=output_%j.txt
#SBATCH --error=error_output_%j.txt
#-----------------------------Other information------------------------
#SBATCH --qos=std
#-----------------------------Required resources-----------------------
#SBATCH --time=60
#SBATCH --mem=2000

#-----------------------------Environment, Operations and Job steps----
#load modules
module load python
pip install cdsapi

#export variables

#your job
#SBATCH --job-name=run_form_download_FAPAR_1999  
time python 'XXX/form_download_FAPAR_1999.py'


