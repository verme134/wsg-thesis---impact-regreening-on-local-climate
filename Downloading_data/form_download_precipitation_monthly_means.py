import cdsapi

c = cdsapi.Client()

c.retrieve(
    'reanalysis-era5-land-monthly-means',
    {
        'format': 'netcdf',
        'variable': 'total_precipitation',
        'product_type': 'monthly_averaged_reanalysis',
        'area': '49.43/73.39/34.51/123.14',
        'year': year_form,
        'month': [
            '01', '02', '03',
            '04', '05', '06',
            '07', '08', '09',
            '10', '11', '12',
        ],
        'time': '00:00',
    },
    outfile_form)


    
