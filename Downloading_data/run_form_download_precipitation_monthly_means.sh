#!/bin/bash
#-----------------------------Mail address-----------------------------
#SBATCH --mail-user=XXX
#SBATCH --mail-type=ALL
#-----------------------------Output files-----------------------------
#SBATCH --output=output_%j.txt
#SBATCH --error=error_output_%j.txt
#-----------------------------Other information------------------------
#SBATCH --qos=std
#-----------------------------Required resources-----------------------
#SBATCH --time=5000
#SBATCH --mem=3600

#-----------------------------Environment, Operations and Job steps----
#load modules
module load python
pip install cdsapi
#export variables
 
#your job

dir_forms='XXX'
dir_data='/XXX/precipitation/monthly_means/'

fyear=1999    
lyear=2019    

cd $dir_forms

for year in $(seq $fyear $lyear); do
   outfile=$dir_data$year'.nc'
   echo $outfile
 
   form='form_download_precipitation_monthly_means_'$year'.py'   
   echo $form   
   
   # Copy the default form and name it to form
   cp form_download_precipitation_monthly_means.py $form
   
   sed -i "s|year_form|'$year'|g" $form
   sed -i "s|outfile_form|'$outfile'|g" $form
   
   python $form	
     
done
   

