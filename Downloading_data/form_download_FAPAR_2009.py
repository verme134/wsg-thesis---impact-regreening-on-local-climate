import cdsapi

c = cdsapi.Client()

c.retrieve(
    'satellite-lai-fapar',
    {
        'variable': 'fapar',
        'satellite': [
            'proba', 'spot',
        ],
        'sensor': 'vgt',
        'horizontal_resolution': '1km',
        'product_version': 'V0',
        'year': [
            '2009', 
        ],
        'month': [
            '01', '02', '03',
            '04', '05', '06',
            '07', '08', '09',
            '10', '11', '12',
        ],
        'nominal_day': [
            '03', '13', '21',
            '23', '24',
        ],
        'area': '49.43/73.39/34.51/123.14',
        'format': 'tgz',
    },
    '/lustre/backup/WUR/ESG/verme134/data/fapar/2009.tar.gz')


    
